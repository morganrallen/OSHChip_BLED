const NUS_SERVICE = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
const NUS_CHAR = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";

var buttons = document.querySelector(".panel");
var button = document.querySelector("button");

var switches = [0, 0, 0, 0];
var characteristic = false;

function sendSwitchState() {
  if(characteristic) {
    characteristic.writeValue(Uint8Array.of(parseInt(switches.join(""), 2)));
  }
}

buttons.addEventListener("click", function(evt) {
  var t = evt.target;

  if(t.classList.contains("button")) {
    t.classList.toggle("on");
    switches[3 - parseInt(t.getAttribute("data-idx"))] = t.classList.contains("on") ? 1 : 0;

    sendSwitchState();
  }
});

function onChar(err, char) {
  if(err) throw err;

  buttons.querySelector(".blue").classList.add("on");
  switches[1] = 1;

  characteristic = char;

  sendSwitchState();
}

function onServiceConnect(err, service) {
  if(err) throw err;

  regret(service.getCharacteristic(NUS_CHAR), onChar);
}

function onGattConnect(err, server) {
  if(err) throw err;

  regret(server.getPrimaryService(NUS_SERVICE), onServiceConnect);
}

// requestDevice must be called from a user "gesture"
button.addEventListener("click", function() {
  regret(navigator.bluetooth.requestDevice({
    acceptAllDevices: true,
    optionalServices: [ "6e400001-b5a3-f393-e0a9-e50e24dcca9e" ]
  }), function(err, device) {
    if(err) throw err;

    regret(device.gatt.connect(), onGattConnect);
  });
}, false);

function regret(p, cb) {
  p.then(function(res) {
    cb(null, res);
  });

  p.catch(function(err) {
    cb(err);
  });
}
